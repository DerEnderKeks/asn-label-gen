import sys

import AveryLabels
import reportlab
from reportlab.lib.units import mm, cm
from reportlab_qrcode import QRCodeImage
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from os import path

asn = 0


def render(c, x, y):
    global asn
    barcode_value = f"ASN{asn:05d}"
    asn = asn + 1

    qr = QRCodeImage(barcode_value, size=y * 0.9)
    qr.drawOn(c, 1 * mm, y * 0.05)
    c.setFont("JetBrains Mono", 2 * mm)
    c.drawString(y, (y - 2 * mm) / 2, barcode_value)


def main():
    if len(sys.argv) < 2:
        print("Start ASN required.")
        return 1

    global asn
    asn = int(sys.argv[1])

    reportlab.rl_config.TTFSearchPath.append(path.expandvars(r'%LOCALAPPDATA%\Microsoft\Windows\Fonts'))
    pdfmetrics.registerFont(TTFont('JetBrains Mono', 'JetBrainsMono[wght].ttf'))

    label = AveryLabels.AveryLabel(4731)
    label_file = "labels4731.pdf"
    print("Writing to %s" % label_file)
    label.open(label_file)
    label.render(render, 189)
    label.close()

    return 0


if __name__ == '__main__':
    sys.exit(main())
