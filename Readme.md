# ASN Label Generator

This is a simple script to create ASN labels for [Paperless](https://docs.paperless-ngx.com/). Mostly based on [this post](https://margau.net/posts/2023-04-16-paperless-ngx-asn/). Currently hard-coded for Avery 4731 labels.

## Installation

1. Install Python
2. Install the [Jetbrains Mono font](https://www.jetbrains.com/lp/mono/) (`variable` variant)
3. Clone this repo
4. `pip install -r requirements.txt`

## Usage

Run it like this, specifying the first ASN:

```shell
python main.py 1
```